## / (index)

## /plane

-Layout and navbar
-List of planes
-login/logout/profile button

## /plane/:planeType

-Layout and navbr
-Seats diagram
-Seats free count
-if logged
    - GO TO RESERVATION -> go to /plane/:idPlane/reservation/:idUser

## /plane/:planeType/reservation

-Layout and navbar
-Seats diagram
-Seats free count
-Seats reserved count
-Reserve seats -> by clicking
    - RESERVE -> if (free) -> all reserved
    - CANCEL -> go to /plane/:idPlane

## * (no match)

-Layout and navbar
-404 message
