import { json } from "react-router-dom";
import { Seat, Reservation } from "./plane";

const APIURL = 'http://localhost:3000/api';


/**
 * A utility function for parsing the HTTP response.
 */
function getJson(httpResponsePromise) {
    // server API always return JSON, in case of error the format is the following { error: <message> } 
    return new Promise((resolve, reject) => {
      httpResponsePromise
        .then((response) => {
          if (response.ok) {
  
            
            // the server always returns a JSON, even empty {}. Never null or non json, otherwise the method will fail
           response.json()
              .then( json => resolve(json) )
              .catch( err => reject({ error: `Cannot parse server response: ${err}`}))
  
          } else {
            console.log("response not ok")
            // analyzing the cause of error
            response.json()
              .then(obj => {
                console.log("obj: " + obj.error)
                reject(obj)}
                ) // error msg in the response body
              .catch(err => reject({ error: `Cannot parse server response: ${err}`})) // something else
          }
        })
        .catch(err => {
            console.log("error")
          reject({ error: `Cannot communicate: ${err}`})}
        ) // connection error
    });
  }

/**
 * Get the full list of planes from the server
 * @returns (A Promise) Array of planes
 */
async function listPlanes() {
    return getJson(fetch(APIURL + '/plane', { credentials: 'include' }));
    // try {
    //     const response = await fetch(APIURL + '/plane', { credentials: 'include' });
    //     if(response.ok) {
    //         const planes = await response.json();
    //         //console.log(planes)
    //         return planes;
    //     } else {
    //         const message = await response.text();
    //         throw new Error(response.statusText + " " + message);
    //     }
    // } catch(err) {
    //     throw new Error(err.message, {cause: err})
    // }
}

async function listSeats(type) {
    return getJson(fetch(APIURL + '/plane/' + type, { credentials: 'include' }))
        .then(json => {
            return json.map((s) => new Seat(s.id, s.code, s.position, s.free, false));
        })
    // try {
    //     const response = await fetch(APIURL + '/plane/' + type, { credentials: 'include' })
    //     if(response.ok) {
    //         const seats = await response.json();
    //         //console.log(seats)
    //         const newSeats = seats.map(s => new Seat(s.id, s.code, s.position, s.free, false))
    //         return newSeats;
    //     } else {
    //         const message = await response.text();
    //         throw new Error(response.statusText + " " + message);
    //     }
    // } catch (err) {
    //     const message = await err.text();
    //     throw new Error(err.statusText + " " + message);
    // }
}

async function listReservations() {
    return getJson(fetch(APIURL+'/myreservation', { credentials: 'include'}))
        .then(json => {
            return json.map(s => new Reservation(s.id, s.planeType, s.seats))
        })
    // try {
    //     const response = await fetch(APIURL+'/myreservation', { credentials: 'include'})
    //     if(response.ok) {
    //         const res = await response.json();
    //         //console.log(seats)
    //         const newReservation = res.map(s => new Reservation(s.id, s.planeType, s.seats))
    //         return newReservation;
    //     } else {
    //         const message = await response.text();
    //         throw new Error(response.statusText + " " + message);
    //     }

    // } catch (err) {
    //     const message = await err.text();
    //     throw new Error(err.statusText + " " + message);
    // }
}

async function deleteReservation(id) {
    console.log("id: " + id)
    return getJson(fetch(APIURL + '/myreservation', { 
        method: 'DELETE',
        headers: {
            'Content-Type': "application/json",
        },
        credentials: 'include',  // this parameter specifies that authentication cookie must be forwared
        body: JSON.stringify({ "id": id })
    }))
}

async function updateSeat(type, ids) {
    const json = await getJson(fetch(APIURL + '/plane/' + type + '/reservation', { 
            method: 'POST',
            headers: {
                'Content-Type': "application/json",
            },
            credentials: 'include',  // this parameter specifies that authentication cookie must be forwared
            body: JSON.stringify({ "planeType": type })
    }));
    console.log("printing json", json);
    if(!json.error) {
        console.log("printing inside");
        for (const id of ids) {
            console.log('/plane/' + type + '/reservation  ' + id + "   PUT ")
            await getJson(fetch(APIURL + '/plane/' + type + '/reservation', {
                method: 'PUT',
                credentials: 'include',
                headers: {
                    'Content-Type': "application/json"
                },
                body: JSON.stringify({ "id": id, "planeType": type, "reservationId": json })
            }))
        }
    }
    //const reservationId = await json.json();
    //                 for (const id of ids) {
    //                     //console.log(APIURL + '/plane/' + type + '/reservation  ' + id + "   PUT " + reservationId)
    //                     await getJson(fetch(APIURL + '/plane/' + type + '/reservation', {
    //                         method: 'PUT',
    //                         credentials: 'include',
    //                         headers: {
    //                             'Content-Type': "application/json"
    //                         },
    //                         body: JSON.stringify({ "id": id, "reservationId": reservationId })
    //                     }))
    //                 }
    //             }
    //         )
    // try {
    //     console.log(APIURL + '/plane/' + type + '/reservation')
    //     const reservation =  await fetch(APIURL + '/plane/' + type + '/reservation', { 
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': "application/json",
    //         },
    //         credentials: 'include',  // this parameter specifies that authentication cookie must be forwared
    //         body: JSON.stringify({ "planeType": type })
    //     });
    //     console.log("After json reservation " + reservation)
            
    //     if(reservation.ok) {
    //         const reservationId = await reservation.json();
    //         console.log("After json reservation " + reservationId)
    //         for (const id of ids) {
    //             console.log(APIURL + '/plane/' + type + '/reservation  ' + id + "   PUT " + reservationId)
    //             const response = await fetch(APIURL + '/plane/' + type + '/reservation', {
    //                 method: 'PUT',
    //                 credentials: 'include',
    //                 headers: {
    //                     'Content-Type': "application/json"
    //                 },
    //                 body: JSON.stringify({ "id": id, "reservationId": reservationId })
    //             })
    //             if(response.ok)
    //                 continue
    //             else {
    //                 const message = await response.text();
    //                 throw new Error(response.statusText + " " + message);
    //             }
    //         }
    //     } else {
    //         const message = await reservation.text();
    //         throw new Error(reservation.statusText + " " + message);
    //     }
    // } catch (err) {
    //     const message = await err.text();
    //     throw new Error(err.statusText + " " + message);
    // }
}

// /**
//  * Get one plane
//  * @returns (A Promise) Array of planes
//  */

// async function getPlane(type) {
//     try {
//         const response = await fetch(APIURL + '/plane/' + type);
//         if(response.ok) {
//             const plane = await response.json();
//             //console.log(planes)
//             return plane;
//         } else {
//             const message = await response.text();
//             throw new Error(response.statusText + " " + message);
//         }
//     } catch(err) {
//         throw new Error(err.message, {cause: err})
//     }
// }

// LOGIN-LOGOUT APIs

// async function checkLogin(username, password) {
//     try {
//         const response = await fetch(APIURL + '/login', {
//             method: 'POST',
//             credentials: 'include',
//             headers: {
//                 'Content-Type': 'application/json'
//             },
//             body: JSON.stringify({
//                 username: username,
//                 password: password
//             })
//         });
//         if (response.ok) {
//             return await response.json();
//         } else {
//             const message = await response.text();
//             throw new Error(response.statusText + " " + message);
//         }
//     } catch (error) {
//         throw new Error(error.message, { cause: error });
//     }
// }
// async function doLogout() {
//     try {
//         const response = await fetch(APIURL + '/logout', {
//             method: 'POST',
//             credentials: 'include',
//         });
//         if (response.ok) {
//             return true ;
//         } else {
//             const message = await response.text();
//             throw new Error(response.statusText + " " + message);
//         }
//     } catch (error) {
//         throw new Error(error.message, { cause: error });
//     }
// }


/**
 * This function wants username and password inside a "credentials" object.
 * It executes the log-in.
 */
const logIn = async (credentials) => {
    console.log("Getting user " + credentials.username + " " + credentials.password);
    return getJson(fetch(APIURL + '/sessions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      credentials: 'include',  // this parameter specifies that authentication cookie must be forwared
      body: JSON.stringify(credentials),
    })
    )
};

/**
 * This function is used to verify if the user is still logged-in.
 * It returns a JSON object with the user info.
 */
const getUserInfo = async () => {
    return getJson(fetch(APIURL + '/sessions/current', {
      // this parameter specifies that authentication cookie must be forwared
      credentials: 'include'
    })
    )
};
/**
 * This function destroy the current user's session and execute the log-out.
 */
const logOut = async() => {
    return getJson(fetch(APIURL + '/sessions/current', {
      method: 'DELETE',
      credentials: 'include'  // this parameter specifies that authentication cookie must be forwared
    })
    )
  }

const API = { listPlanes, listSeats, updateSeat, logOut, logIn, getUserInfo, listReservations, deleteReservation };
export default API;