import { React, useContext, useState, useEffect } from 'react';
import { Row, Col, Button, Spinner, ListGroup, NavLink } from 'react-bootstrap';
import { Link, useParams, useLocation, Outlet, useNavigate } from 'react-router-dom';

import { PlaneList } from './PlaneList';
import { LoginForm } from './Login';
import { SeatForm } from './SeatForm';
import { ReservationCard } from './ReservationCard';
import { RouteLabels } from './LabelsLayout';


import MessageContext from '../MessageContext';

import API from '../API';

function DefaultLayout(props) {
    const location = useLocation();

    const labelId =  (location.pathname === "/myreservation" && 'label-reservations') || (location.pathname === "/" && 'label-planes');
  
    // return (
    //     <Row className="vh-100">
    //     <Col lg={4} xl={2} bg="light" className="below-nav" id="left-sidebar">
    //         <ListGroup as="ul" variant="flush">
    //         <NavLink className="list-group-item" key={'Planes'} to={'/'} style={{ textDecoration: 'none' }}>
    //             Planes
    //         </NavLink>
    //         {<NavLink className="list-group-item" key={'MyReservation'} to={'/myreservation'} style={{ textDecoration: 'none' }}>
    //             MyReservation
    //         </NavLink>
    //         }
    //         </ListGroup>
    //     </Col>
    //     <Col lg={8} xl={10} className="below-nav">
    //         <Outlet/>
    //     </Col>
    //     </Row>
    // );
    return (
      <Row className="vh-100">
        <Col lg={4} xl={2} bg="light" className="below-nav" id="left-sidebar">
          <RouteLabels items={props.labels} selected={labelId} />
        </Col>
        <Col lg={8} xl={10} className="below-nav">
          <Outlet/>
        </Col>
      </Row>
    );
}

function MainLayout(props) {
  
    const {dirty, setDirty, setLoading} = props;
    const location = useLocation();

    const {handleErrors} = useContext(MessageContext);
    
    const { planeType } = useParams();
    const user = useContext(MessageContext) ;
    //const setLoading = props.setLoading;

    // useEffect(() => {
    //     setDirty(true);
    // }, [filterId])

    useEffect(() => {
        if (dirty) {
          API.listPlanes()
            .then(planes => {
              props.setPlane(planes);
              setDirty(false);
            })
            .catch(e => { 
              handleErrors(e); setDirty(false); 
            } ); 
        }
    }, [dirty]);
    
    const planes = props.planes;
    
    //console.log(planeType) ;
    return <>
      { dirty ?
        <Button variant="primary" disabled>
          <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/>
          Loading...
        </Button>
        : <PlaneList planes={planes} /> }
      </>
}

function Reservation(props) {
  const { planeType } = useParams();
  const navigate = useNavigate();

  const myPlane = props.planes.filter((p) => (p.type == planeType))[0];

  //const {handleErrors} = props;
  const {handleErrors} = useContext(MessageContext);
  const {setMessage} = props;
  const handleReservation = async (ids, resSeat) => {
      try{
          let make = true;
          props.setLoading(true);
          console.log("in handleReservation resSeat: " + resSeat + " ids: " + ids.length)
          const seats = await API.listSeats(planeType);
          let toUpdate = [];
          if(ids.length > 0 && resSeat > 0) {
            make = false;
            setMessage("Something went wrong, try again");
            setLoading(false);
            return;
          }
          if(ids.length == 0 && resSeat == 0) {
            make = false;
          }
          if(ids.length > 0 && resSeat == 0) {
            //console.log(seats)
            const unavailableSeats = [];
            toUpdate = ids.map((id) => {
              const seat = seats.find((seat) => seat.id === id);
              if (seat && seat.free) {
                return seat.id; // ID corresponds to a free seat
              } else {
                unavailableSeats.push(seat.id); // ID does not correspond to a free seat
              }
            });
            //unavailableSeats.push(1); //just to force the situation
            if(unavailableSeats.length > 0) {
              // Some IDs do not correspond to free seats
              props.setUnavailableSeats(unavailableSeats);
              console.log(`The following seats are not available: ${unavailableSeats.join(", ")}`);
              make = false;
              setMessage(`Selected seats are not available`);
              //setLoading(false);
            }
          }
          else if(ids.length == 0 && resSeat > 0) {
            console.log("resSeat: " + resSeat)
            toUpdate = seats.filter(s => s.free).map(s => s.id).slice(0, resSeat);
            //console.log("ids: " + ids)
            if (toUpdate.length != resSeat) {
              make = false;
              setMessage("Not enough seats");
            }
          }
          //console.log("update seats")
          if (make) {
          await API.updateSeat(planeType, toUpdate, resSeat);
          console.log("update done")
          props.setDirty(true)
        }
      } catch(err) {
        console.log("err: " + err)
        handleErrors(err);
        props.setLoading(false);
      } finally {
          props.setLoading(false);
          navigate(`/plane/${planeType}`)
      }
  }

  const handleBack = () => {
      navigate(`/plane/${planeType}`)
  }

  return <>
      <SeatForm planes={props.planes} mode={"reserve"} back={handleBack} click={handleReservation} text={"RESERVE"} setDirty={props.setDirty} setLoading={props.setLoading} handleErrors={handleErrors} dirty={props.dirty}/>
  </>
}

function SeatsList(props) {
  const { planeType } = useParams();
  const navigate = useNavigate();
  const {handleErrors} = useContext(MessageContext);

  const handleReservation = () => {
      const {user} = props;
      console.log(user)
      if(user)
          navigate(`/plane/${planeType}/reservation`)
      else 
          return(<div>Please perform <Link to={`/Login`}>login</Link></div>);
  }

  const handleBack = () => {
      navigate(`/`)
  }
  //console.log(": " + props.unavailableSeats)

  return <div>
      <SeatForm plane={props.plane} mode={"display"} back={handleBack} click={handleReservation} text={"GO TO RESERVATION"} setDirty={props.setDirty} setLoading={props.setLoading} unavailableSeats={props.unavailableSeats}/>
  </div>
}

function LoadingLayout(props) {
  return (
    <Row className="vh-100">
      <Col md={4} bg="light" className="below-nav" id="left-sidebar">
      </Col>
      <Col md={8} className="below-nav">
        <h1>Loading ...</h1>
      </Col>
    </Row>
  )
}

function LoginLayout(props) {
  const navigate = useNavigate();
  const handleBack = () => {
    navigate(`/`)
  }
  return (
    <Row className="vh-100">
      <Col md={12} className="below-nav">
        <LoginForm login={props.login} back={handleBack}/>
      </Col>
    </Row>
  );
}

function MyReservations(props) {
  const {handleErrors} = useContext(MessageContext);
  const {dirty, setDirty, loggedIn, setLoading} = props;
  const [reservations, setReservation] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
      API.listReservations().then((list) => {
          setReservation(list);
          setLoading(false);
          setDirty(false)
      })
  }, [dirty]);

  const handleDelete = async (id) => {
      try {
        console.log("handle delete: ")
          setLoading(true);
          await API.deleteReservation(id);
          setDirty(true);
      } catch (err) {
          handleErrors(err);
          console.log("error: " + err.error)
          if(err.status == 401) {
              navigate("/login");
          } 
          setLoading(false);
      } finally {
          setLoading(false);
      }
  }
  return (
    <Row className="vh-100">
      <Col md={12} className="below-nav">
        <ReservationCard loggedIn={loggedIn} handleDelete={handleDelete} reservations={reservations}/>
      </Col>
    </Row>
  );
}

export {MainLayout, Reservation, LoadingLayout, DefaultLayout, SeatsList, LoginLayout, MyReservations}
