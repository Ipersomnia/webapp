
// import { IgrDataGridModule } from 'igniteui-react-grids';
//import { IgrDataGrid } from 'igniteui-react-grids';
// import { IgrTextColumn } from 'igniteui-react-grids';
// import { IgrTemplateColumn } from 'igniteui-react-grids';
// import { IgrTemplateCellInfo } from 'igniteui-react-grids';
// import { IgrTemplateHeader } from 'igniteui-react-grids';
// import { IgrTemplateCellUpdatingEventArgs } from 'igniteui-react-grids';
// import { IgrTemplateHeaderCellUpdatingEventArgs } from 'igniteui-react-grids';

import { useNavigate, useParams } from "react-router-dom";
import './SeatsList.css'
import { Link } from "react-router-dom";
import { Card, Ratio, Form, Alert, Row, Col, Button, Spinner } from 'react-bootstrap';
import { useEffect, useState } from "react";
import API from "../API";


function SeatForm(props) {
    const { planeType } = useParams();

    //const [waiting, setWaiting] = useState(true);
    const [errorMsg, setErrorMsg] = useState('');
    const [gridSeats, setGridSeats] = useState({result: []});
    const [viewGrid, setViewGrid] = useState(false);
    const [resSeat, setResSeat] = useState(0);

    useEffect(() => {
        API.listSeats(planeType).then((list) => {
            setGridSeats(makeGrid(list));
            props.setLoading(false);
            props.setDirty(false);
        })
    }, [props.dirty]);

    /*useEffect(() => {
        if(errorMsg) {
            setTimeout(() => {setErrorMsg('')}, 2000)
        }
    }, [errorMsg])*/

    
    //const myPlane = props.plane.filter((p) => (p.type == planeType))[0]

    const handleErrorMsg = (err) => {
        setErrorMsg(err)
    }

    const handleViewGrid = () => {
        setViewGrid((viewGrid) => (!viewGrid));
        setResSeat(0);
        props.setDirty(true);
    }

    const handleSelection = (id) => {
        setGridSeats(setSelected(id))
    }

    function setSelected(id) {
        const newGridSeat = gridSeats.result.flatMap(row => row.map((s) => (s.id===id ? {...s, selected:!s.selected} : s )));
        return makeGrid(newGridSeat);
    }

    function makeGrid(list) {
        let max_row = 0; //= myPlane.F;
        switch(planeType) {
            case "local":
                max_row = 15;
                break;
            case "regional":
                max_row = 20;
                break;
            case "international":
                max_row = 25;
                break;
            default:
                break;
        }
        let result = [];
        let free = 0;
        let notFree = 0;
        let selected = 0;
        for (let i = 0; i < max_row; i++) {
            const row = list.filter(s => s.code == i+1).sort((s, t) => s.position.charCodeAt(0) - t.position.charCodeAt(0));
            result.push(row);
        }
        result.forEach(row => row.forEach(s => {(s.free? free++:notFree++); s.selected? selected++ : 0}))
        return {total: (free+notFree), free: free, notFree: notFree, selected:selected, result: result};
    }

    return <>
    { props.dirty ?
        <Button variant="primary" disabled>
          <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/>
          Loading...
        </Button>
        : 
        <SeatsDetails gridSeats={gridSeats.result} 
                      total={gridSeats.total} 
                      free={gridSeats.free} 
                      notFree={gridSeats.notFree} 
                      selected={gridSeats.selected} 
                      select={handleSelection}
                      handleErrorMsg={handleErrorMsg}
                      errorMsg={errorMsg}
                      handleViewGrid={handleViewGrid}
                      viewGrid={viewGrid}
                      resSeat={resSeat}
                      setResSeat={setResSeat}
                      props={props}>
            {errorMsg && <Alert variant="danger">{errorMsg}</Alert>}
        </SeatsDetails>}
    </>
}

function getIds(matrix) {
    const res =  matrix.flatMap(row => row.filter(s => s.selected).map(s => s.id));
    //console.log("res: "+ res[0])
    return res;
}

function SeatsDetails(props) {
    const { planeType } = useParams();
    return <>
            <Row xl={2} lg={2} md={2} sm={1} xs={1}>
                <Col className="order-2 order-md-1">
                <Card className="text-center" border="white">
                    {props.props.mode == "display" &&<Card.Header>Available seats - <strong>{planeType}</strong> plane: </Card.Header>}
                    {props.props.mode == "reserve" &&<Card.Header>Reserve seats - <strong>{planeType}</strong> plane: </Card.Header>}
                    {props.props.mode == "display" && <Card.Body >{props.gridSeats.map((row, rowIndex) => <SeatRow key={rowIndex} row={row} select={props.select} mode={props.props.mode} planeType={planeType}  unavailableSeats={props.props.unavailableSeats}/>)}</Card.Body>}
                    {props.props.mode == "reserve" && props.viewGrid && <Card.Body >
                        <Row className="p-3 mb-2">
                            <Col>
                                <div>Select seats</div>
                            </Col>
                            <Col>
                                <div>or</div>
                            </Col>
                            <Col xl={5} md={4}>
                                <Button onClick={props.handleViewGrid}>Insert number</Button>
                            </Col>
                        </Row>
                        <Row></Row>
                        {props.gridSeats.map((row, rowIndex) => <SeatRow key={rowIndex} row={row} select={props.select} mode={props.props.mode} planeType={planeType} unavailableSeats={props.props.unavailableSeats}/>)}</Card.Body>}
                    {props.props.mode == "reserve" && !props.viewGrid && <Card.Body >
                        <Row className="justify-content-center">
                            <Col>
                                <div>Insert number</div>
                            </Col>
                            <Col>
                                <div>or</div>
                            </Col>
                            <Col>
                                <Button onClick={() => props.handleViewGrid(props.viewGrid)}>Select seats</Button>
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col xl={5}>
                            <Form.Group className="p-3">
                                <Form.Control type="number" min={0} max={props.free} step={1} value={props.resSeat}
                                onChange={event => {
                                    let value = parseInt(event.target.value);
                                    if (value > props.free) {
                                        value = props.free; // Set value to the maximum if it exceeds the maximum
                                        }
                                    props.setResSeat(value);}}/>
                            </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>}
                    <Card.Footer>
                        <Row>
                            <Col></Col>
                            <Col>{props.errorMsg}</Col>
                        </Row>
                        <Row className="text-center">
                            <Col>
                                <Button variant="outline-secondary" style={{alignContent: 'center'}} onClick={() => props.props.back()}>BACK</Button>
                            </Col>
                            <Col>
                                {props.props.mode == "display" &&<Button variant="outline-primary" style={{alignContent: 'center'}} onClick={() => {const error = props.props.click(); props.handleErrorMsg(error)}}>{props.props.text}</Button>}
                                {props.props.mode == "reserve" &&<Button variant="outline-primary" style={{alignContent: 'center'}} onClick={() => props.props.click(getIds(props.gridSeats), props.resSeat)}>{props.props.text}</Button>}
                            </Col>
                        </Row>
                    </Card.Footer>
                </Card>
                </Col>
                <Col className="order-1 order-md-2" >
                <Card body border="white text-start" style={{position:'fixed'}}>
                    <div>Total seats: {props.total}</div>
                    <div><i className="bi bi-square-fill" style={{color: '#252850'}}></i> Available seats: {props.free-props.selected-props.resSeat}</div>
                    <div><i className="bi bi-square-fill" style={{color: '#a0a0a0'}}></i> Occupied seats: {props.notFree}</div>
                    {props.props.mode=="reserve" && <div><i className="bi bi-square-fill" style={{color: '#FF4500'}}></i> Selected seats: {props.selected+props.resSeat}</div>}
                </Card>
                </Col>
            </Row>
    </>
}

function SeatRow(props) {
    return (<>
    <Row>
        {props.row.map(
            seat => <SeatCell key={seat.id} mode={props.mode} seat={seat} select={props.select} notAvailable={props.unavailableSeats? props.unavailableSeats.includes(seat.id) : false}/>
        )}
    </Row>
    <Row style={{height: "30px"}}></Row>
    </>
    )
}

function SeatCell(props) {
    const { mode, seat, select} = props;
    const cell = ""+seat.code+seat.position;
    
    //props.available && console.log(cell + ": " + props.notAvailable)
    return <Col>
        {mode=="display" && <Ratio aspectRatio={"1x1"} key={seat.id} style={{maxHeight: "60px", maxWidth: "60px"}}>
            <Card border="white" className={`align-middle text-center ${seat.free ? 'free' : 'occupied cross-container'} ${props.notAvailable ? 'notAvailable' : ''}`}
            style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', fontSize: "12px" }}
            >{cell}</Card>
        </Ratio>}
        {mode=="reserve" && <Ratio aspectRatio={"1x1"} key={seat.id} style={{maxHeight: "60px", maxWidth: "60px"}}>
            <Card border="white" className={`align-middle text-center reserve ${(seat.free && !seat.selected) ? 'free' : 'occupied cross-container'} ${seat.selected ? 'selected' : ''}`}
            style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', fontSize: "12px" }}
            onClick={() => select(seat.id)}
            >{cell}</Card>
        </Ratio>}
    </Col>
}

function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}

export { SeatForm };