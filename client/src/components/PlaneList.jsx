import { Link } from "react-router-dom";
import { Card, CardGroup } from 'react-bootstrap';

function PlaneList(props) {
    return <CardGroup>
        {props.planes.map((p) => {
            return (<Card key = {p.id}>
                <Card.Body>
                <Link to={`/plane/${p.type}`} style={{ textDecoration: 'none', textEmphasisColor: 'blue', color: 'black' }}>
                    <Card.Title>Airplane</Card.Title>
                    <Card.Subtitle>{p.type}</Card.Subtitle>
                    </Link>
                </Card.Body>
            </Card>)
        })}
    </CardGroup>
}

export { PlaneList };