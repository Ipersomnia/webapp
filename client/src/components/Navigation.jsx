import React from 'react';
import 'bootstrap-icons/font/bootstrap-icons.css';
import { useNavigate, useParams } from "react-router-dom";

import { Navbar, Nav, Form, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { LogoutButton, LoginButton } from './Login';

const Navigation = (props) => {

  const {planeType} = useParams();

  return (
    
        <Navbar bg="primary" expand="sm" variant="dark" fixed="top" className="navbar-padding">
            
            <Link to="/" style={{ color: 'white', textDecoration: 'none', fontSize: '40px'}}>
              <i className="bi bi-airplane-engines" style={{padding: '10px'}}/>
              <Navbar.Brand style={{ fontSize: '40px'}}>
                AirplaneList
                {planeType && 
                  <span>
                    - Airplane {planeType}
                  </span>
                }
                </Navbar.Brand>
              </Link>
            <Nav className="my-2 my-lg-0 mx-auto d-sm-block">
            </Nav>
            <Nav className="ml-md-auto">
              <Navbar.Text className="mx-2">
                {props.user && props.user.name && `Welcome, ${props.user.name}!`}
              </Navbar.Text>
              <Form className="mx-2 ml-auto">
                {props.loggedIn ? <LogoutButton logout={props.logout} /> : <LoginButton />}
              </Form>
            </Nav>
        </Navbar>
  );
}

export { Navigation };
