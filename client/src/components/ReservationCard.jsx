import { Link, useNavigate } from "react-router-dom";
import { Card, Ratio, Form, Alert, Row, Col, Button } from 'react-bootstrap';
import { useEffect, useState } from "react";
import API from "../API";
import MessageContext from "../MessageContext";

function ReservationCard(props) {
    const {loggedIn, handleDelete, reservations} = props;

    return <>
    {!loggedIn && <Card text style={{color: 'grey'}}>
        Please login to see your Reservation!
        </Card>}
    {loggedIn && reservations.length == 0 && <Card text style={{color: 'grey'}}>
        No Reservations!
        </Card>}
    {loggedIn && reservations.length != 0 && reservations.map( (reservation, resIndex) => (
        <Card key={reservation.id}>
            <Card.Header className='p-3 mb-2 bg-primary text-white'/>
            <Card.Body>
                <Card.Title>Reservation Details</Card.Title>
                <Card.Subtitle><strong>Reservation on flight:</strong> {reservation.planeType}<br /></Card.Subtitle>
                <Card.Text className="justify-content-md-center p-3">
                    Reserved seats:
                    <Row className="justify-content-md-center p-3">
                        {reservation.seats.map((seat, sIndex) => {
                            const cell = ""+seat.code+seat.position;
                            return <Col key={sIndex} xl={1} sm={2}><Ratio aspectRatio={"1x1"} key={seat.id} style={{maxHeight: "35px", maxWidth: "35px"}}>
                            <Card border="white" className={`align-middle text-center free`}
                            style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', fontSize: "15px", maxWidth: "10px" }}
                            >{cell}</Card>
                        </Ratio></Col>
                        })}
                    </Row>
                </Card.Text>
            </Card.Body>
            <Card.Footer>
                <Button disabled={props.waiting} variant='warning' onClick={() => { handleDelete(reservation.id) }}> <i className="bi bi-trash3"></i> DELETE</Button>{' '}
            </Card.Footer>
        </Card>
        )
    )}
    </>
    
}

export { ReservationCard };