import { Button, Col, Container, Toast, Row, Navbar } from 'react-bootstrap';
import { BrowserRouter, Link, Outlet, Route, Routes, useParams, Navigate, useNavigate } from 'react-router-dom';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import './App.css';

import { useContext, useEffect, useState, React } from "react";

import { MainLayout, Reservation, LoadingLayout, DefaultLayout, SeatsList, LoginLayout, MyReservations } from './components/PageLayout';

import { PageNotFound } from './components/PageNotFound';
import { Navigation } from './components/Navigation';


import API from './API';
import MessageContext from './MessageContext';


function App() {
  const [dirty, setDirty] = useState(true);

  // This state contains the list of planes (it is initialized from a predefined array).
  const [planes, setPlanes] = useState([]);
  
  // This state keeps track if the user is currently logged-in.
  const [loggedIn, setLoggedIn] = useState(false);
  // This state contains the user's info.
  const [user, setUser] = useState(null);

  const [loading, setLoading] = useState(false);

  const [message, setMessage] = useState('');

  const [unavailableSeats, setUnavailableSeats] = useState(false);


  // If an error occurs, the error message will be shown in a toast.
  const handleErrors = (err) => {
    let msg = '';
    console.log("unavailable: " + unavailableSeats)
    console.log("error: " + err)
    if (err.error){ msg = err.error; console.log("msg: " + msg)}
    else if (String(err) === "string") {msg = String(err); console.log("msg: " + msg)}
    else msg = 'Unknown Error';
    setMessage(msg); // WARN: a more complex application requires a queue of messages. In this example only last error is shown.
  }
  const labels = {
    'label-planes':       { label: 'All planes', url: ''},
    'label-reservations':  { label: 'MyReservation', url: '/myreservation'}
  };

  /**
   * This function handles the login process.
   * It requires a username and a password inside a "credentials" object.
   */
  const handleLogin = async (credentials) => {
    try {
      const user = await API.logIn(credentials);
      setUser(user);
      setLoggedIn(true);
    } catch (err) {
      // error is handled and visualized in the login form, do not manage error, throw it
      throw err;
    }
  };

  useEffect(() => {
        if(unavailableSeats) {
            setTimeout(() => {setUnavailableSeats(false)}, 5000)
        }
    }, [unavailableSeats])

  /**
     * This function handles the logout process.
     */ 
  const handleLogout = async () => {
    await API.logOut();
    setLoggedIn(false);
    // clean up everything
    setUser(null);
  };

  useEffect(() => {
    const init = async () => {
      try {
        setLoading(true);
        const user = await API.getUserInfo();  // here you have the user info, if already logged in
        setUser(user);
        setLoggedIn(true); setLoading(false);
      } catch (err) {
        handleErrors(err); // mostly unauthenticated user, thus set not logged in
        setUser(null);
        setLoggedIn(false); setLoading(false);
      }
    };
    init();
  }, []);  // This useEffect is called only the first time the component is mounted.


  useEffect(() => {
    // load the list of planes from the API server
    API.listPlanes().then((list) => {
      setPlanes(list);
    })
  }, []);

  return (
  <BrowserRouter>
    <MessageContext.Provider value={{handleErrors}}>
      <Container fluid className="App">
        <Navigation logout={handleLogout} user={user} loggedIn={loggedIn} />

        <Routes>
          <Route path="/" element={loading ? <LoadingLayout /> : <DefaultLayout labels={labels}/>}>
            <Route index element={<MainLayout planes={planes} dirty={dirty} setDirty={setDirty} setPlanes={setPlanes} setLoading={setLoading} labels={labels}/>} />
            <Route path='plane/:planeType'
              element={<SeatsList planes={planes} user={user} dirty={dirty} setDirty={setDirty} setLoading={setLoading} unavailableSeats={unavailableSeats}/>} />
            <Route path='plane/:planeType/reservation'
              element={loggedIn ? <Reservation planes={planes} user={user} dirty={dirty} setDirty={setDirty} setMessage={setMessage} setLoading={setLoading} setUnavailableSeats={setUnavailableSeats}/> : <Navigate replace to='/login'/>} />
            <Route path='myreservation'
              element={<MyReservations planes={planes} loggedIn={loggedIn} user={user} dirty={dirty} setDirty={setDirty} handleErrors={handleErrors} setLoading={setLoading}/>} />
            <Route path='*' element={<PageNotFound />} />
          </Route>
          <Route path="/login" element={!loggedIn ? <LoginLayout login={handleLogin} /> : <Navigate replace to='/' />} />
        </Routes>


        <Toast show={message !== ''} onClose={() => setMessage('')} delay={4000} autohide bg="danger">
          <Toast.Body>{message}</Toast.Body>
        </Toast>
      </Container>
    </MessageContext.Provider>
  </BrowserRouter>
  );
}

export default App
