'use strict' ;

//local(F=15, P=4)
//regional (F=20, P=5)
//international( F=25, P=6)

function Seat(id, code, position, free, selected) {
    this.id = id;
    this.code = code;
    this.position = position;
    this.free = free;
    this.selected = selected;
}

function Plane(id, type) {
    this.id = id;
    this.type = type;
    this.F = 0;
    this.P = 0;
    this.all_seats = [];

    this.new = function(plane) {
        let F, P;
        switch(type) {
            case "local":
                F = 15; P = 4;
            break;
            case "regional":
                F = 20, P = 5;
            break;
            case "international":
                F = 25; P = 6;
            break;
        }

        this.id = plane.id;
        this.type = plane.type;
        this.F = F;
        this.P = P;
    }

    this.add = function(seats) {
        this.seats.push({...seats, planeId:id})
    }

    this.getSeat = function(seat) {
        return this.all_seats.filter((s) => s.id == seat.id);
    }

    this.getAllSeats = function() {
        return this.all_seats;
    }

    this.selectSeat = function(seat) {
        this.all_seats.filter((s) => s.id == seat.id).forEach(s.selected = true);
    }

    this.reserveAll = function() {
        this.all_seats.forEach((s) => {
            if (s.selected)
                s.free = false;
                s.selected = false;
        })
    }

    this.releaseAll = function() {
        this.all_seats.forEach((s) => {
            if (s.selected)
                s.selected = false;
        })
    }
}

function Reservation(idReservation, planeType, seats) {
    this.id = idReservation; 
    this.planeType = planeType;
    this.seats = seats;
}

export {Plane, Seat, Reservation};