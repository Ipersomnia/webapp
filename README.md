[![Review Assignment Due Date](https://classroom.github.com/assets/deadline-readme-button-24ddc0f5d75046c5622901739e7c5dd533143b0c8e959d652212380cedb1ea36.svg)](https://classroom.github.com/a/Ij4wZ9xX)
# Airplane Seats

## Student: GRUBER AURORA 

# Server side

## API Server

- POST `/api/sessions`
  - body: credentials
  - user
- GET `/api/sessions/current`
  - None
  - user
- DELETE `/api/sessions/current`
  - None
  - None
- GET `/api/planes`
  - none
  - list of all planes or an error
- GET `/api/planes/:planeType`
  - body: planeType
  - list of all seats of that plane or an error
- PUT `/api/plane/:planeType/reservation`
  - body: id (of the seat), reservationId
  - empty or an error
- POST `/api/plane/:planeType/reservation`
  - param: planeType, body: planeType
  - reservationId or an error
- DELETE `/api/myreservation`
  - body: id(of the reservation)
  - None or an error

## Database Tables

- Table `users` - contains id email password salt name
- Table `reservation` - contains id userId planeType
- Table `seats` - contains id planeId code position free reservationId
- Table `planes` - contains id type
# Client side


## React Client Application Routes

- Route `/`: lists of all planes, user can choose
- Route `/plane/:planeType`: lists of all seats of a plane (legend with free, occupied), param says which is the plane
- Route `/plane/:planeType/reservation`: page where the logged user can reserve the seats -> button to choose between inserting number of seats os selectin seats on the gird, param specify the planeType
- Route `/myreservation`: list of all reservation of logged user (he can also delete them)
- Route `/login`: login form (user can perform login)


## Main React Components

- `DefaultLayout` (in `PageLayout.jsx`): component purpose and main functionality
- `MainLayout` (in `PageLayout.jsx`): displays the plane calling
  - `PlaneList` (in `PlaneList.jsx`)
- `SeatsList` (in `PageLayout.jsx`): handles navigation and calls SeatForm
- `Reservation` (in `PageLayout.jsx`): handles reservations and calls SeatForm
- `SeatForm` (in `SeatForm.jsx`): has 2 modes: display -> displays the seats grid ecc.
                                              reserve -> handles reserving selection
- `MyReservations` (in `PageLayout.jsx`): displays all the reservation of a user calling
  - `ReservationCard`  (in `ReservationCard.jsx`)
- `LoginLayout` (in `PageLayout.jsx`): displays login form and handles login calling
  - `LoginForm` (in `Login.jsx`): component purpose and main functionality

(only _main_ components, minor ones may be skipped)

# Usage info

## Example Screenshot

![booking_form](./img/booking_form.png)
![booking_grid](./img/booking_grid.png)

## Users Credentials

- User1, user1@email.com, password
- User2, user2@email.com, password
- User3, user3@email.com, password
- User4, user4@email.com, password
