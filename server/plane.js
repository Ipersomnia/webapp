'use strict' ;

//local(F=15, P=4)
//regional (F=20, P=5)
//international( F=25, P=6)

function Plane(id, type) {
    this.id = id;
    this.type = type;
}

function Seat(id, plane_id, code, position, free) {
    this.id = id;
    this.plane_id = plane_id;
    this.code = code;
    this.position = position;
    this.free = free;
}

function Reservation(id, planeType) {
    this.id = id; 
    this.planeType = planeType;
    this.seats = [];

    this.add = function(allSeats) {
        return this.seats.push(...allSeats);
    }
}


exports.Plane = Plane;
exports.Seat = Seat;
exports.Reservation = Reservation;