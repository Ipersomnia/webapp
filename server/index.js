'use strict';

const PORT = 3000;

// logging middleware
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');


const session = require('express-session');

// validation middleware
const { check, validationResult, } = require('express-validator'); 

// passport
const passport = require('passport');
const LocalStrategy = require('passport-local');

const dao = require('./plane-dao');
const userdao = require('./user-dao');
const {Plane, Seats} = require('./plane');
//const session = require('express-session');


const app = express();
app.use(morgan('dev'));

//app.use(morgan('combined'));
app.use(express.json());
app.use(cors(
   {
    origin: 'http://127.0.0.1:5173',
    credentials: true,
}
));

// see: https://expressjs.com/en/resources/middleware/session.html
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
}));

passport.use(new LocalStrategy(async function verify(username, password, callback) {
    // verify function
    const user = await userdao.getUser(username, password)
    if(!user)
      return callback(null, false, 'Incorrect username or password');  
      
    return callback(null, user);
    // NOTE: user info in the session (all fields returned by userDao.getUser, i.e, id, username, name)
}));

passport.serializeUser((user, callback) => {
    callback(null, user);
});
passport.deserializeUser((user, callback) => {
    callback(null, user);
});

//app.use(passport.initialize());
app.use(passport.authenticate('session'));

// Custom middleware: check login status
const isLogged = (req, res, next) => {
    if (req.isAuthenticated()) {
    //if (true) { //finchè non funziona isAuth
        next();
    } else {
        res.status(401).send({error: 'Not authorized'});
    }
}

/*** Utility Functions ***/

// This function is used to format express-validator errors as strings
const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
    return `${location}[${param}]: ${msg}`;
};

/*** Users APIs ***/

// POST /api/sessions 
// This route is used for performing login.
app.post('/api/sessions', function(req, res, next) {
    //console.log("here")
    passport.authenticate('local', (err, user, info) => { 
      if (err)
        return next(err);
        if (!user) {
          // display wrong login messages
          return res.status(401).json({ error: info });
        }
        // success, perform the login and extablish a login session
        req.login(user, (err) => {
          if (err)
            return next(err);
          
          // req.user contains the authenticated user, we send all the user info back
          // this is coming from userDao.getUser() in LocalStratecy Verify Fn
          return res.json(req.user);
        });
    })(req, res, next);
});

// GET /api/sessions/current
// This route checks whether the user is logged in or not.
app.get('/api/sessions/current', (req, res) => {
    if(req.isAuthenticated()) {
      res.status(200).json(req.user);}
    else
      res.status(401).json({error: 'Not authenticated'});
});
  
// DELETE /api/session/current
// This route is used for loggin out the current user.
app.delete('/api/sessions/current', (req, res) => {
    req.logout(() => {
      res.status(200).json({});
    });
});
  

/******* LOGIN - LOGOUT OPERATIONS *******/

// POST /api/login
// app.post('/api/login', passport.authenticate('local'), (req, res) => {
//     res.json(req.user);
// });

// // POST /api/logout
// app.post('/api/logout', (req, res) => {
//     req.logout(()=>{res.end()});
// })

/******* PUBLIC APIs (NO AUTHENTICATION) *******/

// GET /api/plane
// List all planes
app.get('/api/plane', (req, res) => {
    dao.listPlanes().then((result) => {
        res.json(result);
    }).catch((error) => {
        res.status(500).json(error);
    });
});

// GET /api/plane
// List all seats of the plane
app.get('/api/plane/:planeType', (req, res) => {
    dao.selectPlaneSeats(req.params.planeType).then((result) => {
        res.json(result);
    }).catch((error) => {
        res.status(500).json(error);
    });
});

/******* PRIVATE APIs (REQUIRE AUTHENTICATION) *******/

//app.use(isLogged);  // middleware installed for the APIs below this line

// PUT /api/plane/:planeType/reservation
// Replace a seat with new values 
app.put('/api/plane/:planeType/reservation', isLogged,
[
  check('id').isInt(),  
  check('reservationId').isInt(),
], async (req, res) => {
    // Is there any validation error?
    const errors = validationResult(req).formatWith(errorFormatter); // format error message
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array().join(", ")  }); // error message is a single string with all error joined together
    }
    try { 
        console.log("updating (put)")
        await dao.reserveSeats(req.body.id, req.body.reservationId);
        return res.status(200).json({});
    } catch (err) {
        await dao.freeSeat(req.body.id);
        await dao.delReservation(req.body.id);
        res.status(500).json({error: `${err}`});
    }
});

//POST
//Insert a new reservation in the db
app.post('/api/plane/:planeType/reservation', isLogged, [
    check('planeType').isLength({min: 1, max:160}),
  ], async (req, res) => {
    // Is there any validation error?
    const errors = validationResult(req).formatWith(errorFormatter); // format error message
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array().join(", ") }); // error message is a single string with all error joined together
    }
    try { 
        console.log("inserting reservation")
        if (req.body.planeType !== req.params.planeType) {
            return res.status(422).json({ error: 'URL and body id mismatch' });
        }
        const id = await dao.createReservation(req.user.id, req.params.planeType);
        console.log("Reservation ID:", id);
        res.json(id);
    } catch (err) {
        return res.status(500).json({ error: `${err}` });
    }
});

//delete a reservation
app.delete('/api/myreservation', isLogged,
[ check('id').isInt() ], async (req, res) => {
    try { 
        console.log("deleting reservation")
        await dao.freeSeat(req.body.id);
        await dao.delReservation(req.body.id);
        res.status(200).json({});
    } catch (err) {
        res.status(500).json({error: `Database error during the deletion of the reservation: ${err}`});
    }
})

//get reservations
app.get('/api/myreservation', isLogged, async (req, res) => {
    console.log("getting reservations")
    dao.getReservation(req.user.id).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(500).json(err);
    });
})

app.listen(PORT,
    () => { console.log(`Server started on http://localhost:${PORT}/`) });