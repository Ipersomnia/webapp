'use strict' ;
const sqlite = require('sqlite3');
const { Plane, Seat } = require('./plane');


const db = new sqlite.Database('airplane.sqlite', (err) => {
    if (err) throw err;
});

//local(F=15, P=4)
//regional (F=20, P=5)
//international( F=25, P=6)

//creo l'ambiente in cui vado a lavorare:
//  1. Creo tabella sedili
//  2. Creo tabella aerei
//  3. Creo aereo + posti

db.serialize(() => {
    //0 user
    db.run(`
    CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      email TEXT NOT NULL,
      password TEXT NOT NULL,
      salt TEXT NOT NULL,
      name TEXT NOT NULL
    )`, (err) => {
        if(err) {
            throw ('Error creating table seats: ', err);
        }
        else { 
            const query = `INSERT INTO users (email, password, salt, name) VALUES (?,?,?,?)`;
            const userQuery = 'SELECT * FROM users WHERE email=?';
            db.get(userQuery, ["user1@email.com"], (err, row) => { //generare un salt e poi un hashed
                if(err) {
                    throw ('Error insertin user: ', err);
                }
                else {
                    if (!row) {
                        const salt = 'b3d50d93beddfb3e';
                        const user = "User1";
                        const email = "user1@email.com";
                        const password = '969b0b0ec9ecf7fbb2f77bbf11b62f4258625605690313f9e34e433aec7fa379'; //user1
                        db.run(query, [email, password, salt, user], function (err) {
                            if (err) {
                                throw ('Error inserting User: ', err);;
                            }
                            else{
                                console.log("User created");
                            }
                        })
                    };
                }
            })
            db.get(userQuery, ["user2@email.com"], (err, row) => { //generare un salt e poi un hashed
                if(err) {
                    throw ('Error inserting user: ', err);
                }
                else {
                    if (!row) {
                        const salt = '9669574b6370e814';
                        const user = "User2";
                        const email = "user2@email.com";
                        const password = '77ef79874de4ce255d9ed0bd5b48973170813f59affcf238f3728a7a8522bf46'; //user1
                        db.run(query, [email, password, salt, user], function (err) {
                            if (err) {
                                throw ('Error inserting User: ', err);;
                            }
                            else{
                                console.log("User created");
                            }
                        })
                    };
                }
            })
            db.get(userQuery, ["user3@email.com"], (err, row) => { //generare un salt e poi un hashed
                if(err) {
                    throw ('Error inserting user: ', err);
                }
                else {
                    if (!row) {
                        const salt = '0c98c87cbe39a1f7';
                        const user = "User3";
                        const email = "user3@email.com";
                        const password = '5266cf6d9608df70649ac811d82f872284730c7f408b0898cdf872d98112515a'; //user1
                        db.run(query, [email, password, salt, user], function (err) {
                            if (err) {
                                throw ('Error inserting User: ', err);;
                            }
                            else{
                                console.log("User created");
                            }
                        })
                    };
                }
            })
            db.get(userQuery, ["user4@email.com"], (err, row) => { //generare un salt e poi un hashed
                if(err) {
                    throw ('Error inserting user: ', err);
                }
                else {
                    if (!row) {
                        const salt = '8ddc7df9db322a76';
                        const user = "User4";
                        const email = "user4@email.com";
                        const password = '2aaec1dd7865a838d6ab82ce9a5c0290a74e3c1be45be96546eb61d24e571546'; //user1
                        db.run(query, [email, password, salt, user], function (err) {
                            if (err) {
                                throw ('Error inserting User: ', err);;
                            }
                            else{
                                console.log("User created");
                            }
                        })
                    };
                }
            })
        }
    });

    //1 reservation
    db.run(`
    CREATE TABLE IF NOT EXISTS reservation (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      userId INTEGER NOT NULL,
      planeType TEXT NOT NULL,
      FOREIGN KEY (userId) REFERENCES users (id)
      FOREIGN KEY (planeType) REFERENCES plane (type)
    )`, (err) => {
        if(err) {
            throw ('Error creating table reservation: ', err);
        }
    });

    //1 seats
    db.run(`
    CREATE TABLE IF NOT EXISTS seats (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      planeId INTEGER NOT NULL,
      code INTEGER NOT NULL,
      position CHAR NOT NULL,
      free BOOL NOT NULL,
      reservationId INTEGER,
      FOREIGN KEY (reservationId) REFERENCES reservation(id),
      FOREIGN KEY (planeId) REFERENCES planes(id)
    )`, (err) => {
        if(err) {
            throw ('Error creating table seats: ', err);
        }
        else { 
            console.log("Table seats created");
        }
    });

    //2 plane
    const createTablePlane = `
    CREATE TABLE IF NOT EXISTS planes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    type TEXT NOT NULL
    )`;
    db.run(createTablePlane, (err) => {
        if (err) {
            throw ('Error creating table planes: ', err);
        }
        else {
            console.log("Table planes created");

            const query = `INSERT INTO planes (type) VALUES (?)`;
            const planeTypeQuery = 'SELECT * FROM planes WHERE type=?'
            const seatQuery = `INSERT INTO seats (planeId, code, position, free)
                               VALUES (?, ?, ?, ?)`;

            //3 plane local
            db.get(planeTypeQuery, ["local"], (err, row) => {
                if (err)
                    throw ("Error in accessing table planes: ",  err);
                else {
                    if (!row) {
                        const planeLoc = new Plane(null, "local");
                        const rows = 15;
                        const col = 4;

                        db.run(query, [planeLoc.type], function (err) {
                            if (err) {
                                throw ('Error inserting planeLoc: ', err);;
                            }
                            else{
                                console.log("PlaneLoc created");
                                const planeId = this.lastID; //last planeId inserted
                                
                                //create and insert all the seats
                                for (let i = 0; i < rows; i++) {
                                    let c = 'A';
                                    for(let j = 0; j < col; j++) {
                                        const seat = new Seat(null, planeId, i+1, c, true);
                                        
                                        db.run(seatQuery, [seat.plane_id, seat.code, seat.position, seat.free], (err) => {
                                            if (err) {
                                                throw ('Error inserting seat ' + seat.code + seat.position +": ", err);
                                            }
                                        })
                                        c = nextChar(c);
                                    }
                                }
                            }
                          });
                    }
                }
            })
            //3 plane regional
            db.get(planeTypeQuery, ["regional"], (err, row) => {
                if (err)
                    throw ("Error in accessing table planes")
                else {
                    if (!row) {
                        const planeReg = new Plane(null, "regional")
                        const rows = 20;
                        const col = 5;
                        db.run(query, [planeReg.type], function (err) {
                            if (err) {
                                throw ('Error inserting planeReg:', err);;
                            }
                            else{
                                console.log("planeReg created");
                                const planeId = this.lastID; //last planeId inserted
                                
                                //create and insert all the seats
                                for (let i = 0; i < rows; i++) {
                                    let c = 'A';
                                    for(let j = 0; j < col; j++) {
                                        const seat = new Seat(null, planeId, i+1, c, true);
                                        
                                        db.run(seatQuery, [seat.plane_id, seat.code, seat.position, seat.free], (err) => {
                                            if (err) {
                                                throw ('Error inserting seat ' + seat.code + seat.position +": ", err);
                                            }
                                        })
                                        c = nextChar(c);
                                    }
                                }
                            }
                        });
                    }

                }
            })
            //3 plane international
            db.get(planeTypeQuery, ["international"], (err, row) => {
                if (err)
                    throw ("Error in accessing table planes")
                else {
                    if (!row) {
                        const planeInt = new Plane(null, "international")
                        db.run(query, [planeInt.type], function (err) {
                            if (err) {
                                throw ('Error inserting planeInt:', err);;
                            }
                            else {
                                console.log("PlaneInt created");
                                const planeId = this.lastID; //last planeId inserted
                                const rows = 25;
                                const col = 6;
                                //create and insert all the seats
                                for (let i = 0; i < rows; i++) {
                                    let c = 'A';
                                    for(let j = 0; j < col; j++) {
                                        const seat = new Seat(null, planeId, i+1, c, true);
                                        db.run(seatQuery, [seat.plane_id, seat.code, seat.position, seat.free], (err) => {
                                            if (err) {
                                                throw ('Error inserting seat ' + seat.code + seat.position +": ", err);
                                            }
                                        })
                                        c = nextChar(c);
                                    }
                                }
                            }
                        });
                    }
                }
            })
        }
    })

})

function nextChar(c) {
    return String.fromCharCode(c.charCodeAt(0) + 1);
}

module.exports = db ; // a 'default export' in nodejs conventions