'use strict' ;

const {Plane, Seat, Reservation} = require('./plane');

const db = require('./db');
const { resolve } = require('path');

exports.checkPlaneType = function(type) {
    if (type !== "international" || type !== "regionale" || type !== "local")
        return false;
    return true;
}

// exports.createSeat = function( code, position) {
//     return new Promise((resolve, reject) => {
//         const sql = 'INSERT INTO seat(id, code, position) VALUES(?,?)'
//         db.run(sql, [id, code, position], (err) => {
//             if (err)
//                 reject(err.message);
//             else
//                 resolve(true);
//         })
//     })
// }

exports.createReservation = (userId, planeType) => {
    return new Promise((resolve, reject) => {
        console.log("createReservation " + userId)
        const getsql = 'SELECT * FROM reservation WHERE userId=? AND planeType=?'
        const sql = 'INSERT INTO reservation (userId, planeType) VALUES (?,?)';
        db.get(getsql, [userId, planeType], (err, row) => {
            if (err)
                reject(err.message);
            else {
                if(!row) {
                    db.run(sql, [userId, planeType], function (err) {
                        if (err)
                            reject(err.message);
                        else {
                            const id = this.lastID;
                            console.log("LastID " + id)
                            resolve(id); //
                        }
                    })
                }
                else {
                    const errorMessage = "You already have a reservation on this plane: " + planeType;
                    const error = new Error(errorMessage);
                    reject(error);
                }
            }
        })
    })
};

exports.getReservation = (userId) => {
    return new Promise((resolve, reject) => {
      const sql = 'SELECT * FROM reservation WHERE userId=?';
      db.all(sql, [userId], async (err, rows) => {
        if (err) {
          reject(err.message);
        } else {
          if (rows) {
            const reservations = [];
            for (const row of rows) {
                const reservation = new Reservation(row.id, row.planeType);
                //console.log(row.id)
                const seats = await this.selectSeats(reservation.id);
                console.log(seats);
                reservation.add(seats);
                reservations.push(reservation);
                }
            resolve(reservations);
          } else {
            resolve([]);
          }
        }
      });
    });
  };
  

exports.delReservation = (id) => {
    console.log("ResId: " + id)
    return new Promise((resolve, reject) => {
        const sql = 'DELETE FROM reservation WHERE id=?'
        db.run(sql, [id], (err) => {
            if (err)
                reject(err.message);
            else
                resolve(true);
        })
    })
};

exports.selectPlane = (type) => {
    return new Promise((resolve, reject) => {
        const sql = "SELECT * FROM planes WHERE type = ?"
        db.get(sql, [type], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(new Plane(row.id, row.type, row.F, row.P));
            }
        });
    })
};

exports.listPlanes = () => {
    return new Promise((resolve, reject) => {
        const sql = "SELECT * FROM planes"
        db.all(sql, (err, row) => {
            if (err) {
                reject(err);
            } else {
                const planes = row.map((p) => new Plane(p.id, p.type, p.seats_id))
                resolve(planes);
            }
        });
    })

};

exports.selectPlaneSeats = (type) => {
    return new Promise((resolve, reject) => {
        const sql = `SELECT seats.id, seats.planeId, seats.code, seats.position, seats.free
                     FROM seats, planes WHERE planes.id = seats.planeId AND planes.type = ?`;
        db.all(sql, [type], (err, rows) => {
            if (err)
                reject(err)
            else {
                const seats = rows.map((s) => new Seat(s.id, s.planeId, s.code, s.position, s.free));
                resolve(seats);
            }
        });
    });
};

exports.selectSeats = (reservationId) => {
    return new Promise((resolve, reject) => {
        const sql = "SELECT * FROM seats WHERE reservationId = ?"
        //console.log(reservationId)
        db.all(sql, [reservationId], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                console.log(rows)
                resolve(rows.map(row => new Seat(row.id, null, row.code, row.position, row.free)));
            }
        });
    })
};

exports.reserveSeats = (id, resId) => {
    return new Promise((resolve, reject) => {
        const sql = `UPDATE seats
        SET free=false, reservationId=?
        WHERE id=?` ;
        console.log("waiting to run")
        db.run(sql, [resId, id], (err)=>{
            if(err) {
                console.error("Error updating seat:", err);
                reject(err) ;
            } else {
                console.log("Seat updated successfully");
                resolve(true) ;
            }
        }) ;
    })
};

exports.freeSeat = (id) => {
    return new Promise((resolve, reject) => {
        const sql = `UPDATE seats
        SET free=true, reservationId=null
        WHERE reservationId=?` ;
        console.log("waiting to run")
        db.run(sql, [id], (err)=>{
            if(err) {
                console.error("Error updating seat:", err);
                reject(err) ;
            } else {
                console.log("Seat updated successfully");
                resolve(true) ;
            }
        }) ;
    })
};